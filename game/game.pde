Star star;

int starCount=50;                      // number of stars to instantiate
Star[] stars=new Star[starCount];      // array to hold the star objects
float minDiameter=3;
float maxDiameter=8;

Player playerOne;


void setup(){
// set canvas size
  size (400, 400);
  
  //set the framerate
  frameRate(60);
  
  //instantiate stars to fill the stars array
  for (int i=0; i<stars.length; i++) {
    stars[i]=new Star(random(minDiameter, maxDiameter));
  }
playerOne = new Player();
}


void draw(){
  // update each star
  for (int i=0; i<stars.length; i++) {
    stars[i].update();
  }
  playerOne.update();
  
  //blank the background
  background(0);
  
  //display each star
  for (int i=0; i<stars.length; i++) {
    stars[i].display();
  }

 playerOne.display();
}
