class Player {

  PVector position;

  Player() {
    position = new PVector();
  }

  void update() {
    position.set(mouseX, mouseY);
  }

  void display() {
    //set the drawing mode for rectangles
    rectMode(CENTER);

    //set the fill colour to white
    fill(255);
    //draw a rectangle that follows the mouse
    rect(mouseX, mouseY, 40, 40);

    // set the fill color to red
    fill(255, 0, 0);
    // draw a circle that will trail the main body
    ellipse(pmouseX, pmouseY, 20, 20);
  }
}
