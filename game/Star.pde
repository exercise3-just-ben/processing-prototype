class Star {
  PVector position;
  float diameter;
  
  Star(float tempDiameter) {
    position=new PVector (random(width), random(height));
    diameter=tempDiameter;
  }
  
  
  void update() {
  }
  
  void display() {
    // set the fill colour to a random colour to create a flickering effects
    fill(random(100, 255));
    ellipse(position.x, position.y, diameter, diameter);
  }
}
